﻿using UnityEngine;
using System.Collections;
using System;

public class grass_waving_script : MonoBehaviour {
    public float bendFactor;

    private float _enterOffset;
    private float _exitOffset;
    private bool _isBending;
    private bool _isRebounding;
    public float _colliderHalfWidth;
    MeshFilter _meshfilter;
	// Use this for initialization
	void Start () {
        _meshfilter = GetComponent<MeshFilter>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    private float map(float offset, float v1, float radius, int v2, float v3)
    {
        throw new NotImplementedException();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            _enterOffset = other.transform.position.x - transform.position.x;
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        var offset = other.transform.position.x - transform.position.x;
        if ( _isBending||Mathf.Sign(_enterOffset) != Mathf.Sign(offset))//ham mathf.sin quy ve  0,duong =1 am =-1
        {
            _isRebounding = false;
            _isBending = true;

            var Radius = _colliderHalfWidth+ other.bounds.size.x * 0.5f;
            _exitOffset =  map(offset, -Radius, Radius, -1, 1f);
            setVertHoriozontalOffset(_exitOffset);
        }
    }
    void setVertHoriozontalOffset(float offset)
    {
        var verts = _meshfilter.mesh.vertices;
        verts[1].x = 0.5f + offset * bendFactor / transform.localScale.x;
        verts[3].x = -0.5f + offset * bendFactor / transform.localScale.x;
        _meshfilter.mesh.vertices = verts;
    }

   
}
