﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxScripts : MonoBehaviour {
    public Transform cameraTransform;
    [SerializeField]
    private float ParallaxSpeed;
    private float lastCameraX;
	// Use this for initialization
	void Start () {

        cameraTransform = GameObject.Find("CameraPosX").transform;
        lastCameraX = cameraTransform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        float deltaX = cameraTransform.position.x - lastCameraX;
        transform.position += Vector3.right * (((deltaX) * ParallaxSpeed  )*Time.fixedDeltaTime); // cach don gian de nhan vector3
        lastCameraX = cameraTransform.position.x;
		
	}
}
