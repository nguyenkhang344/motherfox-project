﻿using UnityEngine;
using System.Collections;

public class effectDestroy : MonoBehaviour {
    void OnEnable()
    {
        Invoke("Destroy", 0.7f); // tac dung tuong tu waitforseccond
    }
    void Destroy()
    {
        gameObject.SetActive(false);
    }
    void OnDisable()
    {
        CancelInvoke();
    }

}
