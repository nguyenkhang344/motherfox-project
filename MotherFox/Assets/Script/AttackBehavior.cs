﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Complete {
    public class AttackBehavior : MonoBehaviour
    {
        [HideInInspector]
        public Animator anim;
        public AnimationsStats animStats;
        public EnemyStats enemyStates;
        public StateController controller;

       

        // Use this for initialization
        void Start()
        {
            anim = GetComponent<Animator>();
            controller = GetComponent<StateController>();           
        }


        public void Attack(StateController controller)
        {

            anim.SetBool(animStats.attack, true);
         

        }
        public void StopAttack(StateController controller)
        {
            anim.SetBool(animStats.attack,false);
          
        } 
       
    }
   
}
