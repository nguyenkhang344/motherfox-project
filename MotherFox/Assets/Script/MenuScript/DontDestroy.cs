﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroy : MonoBehaviour {
    public static DontDestroy instance;
    Transform playerTurn;
    void Awake()
    {
        if (instance != null)
        {
            if(instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        
    }
    void Start()
    {
       playerTurn = this.gameObject.transform.GetChild(0);
    }
    void FixedUpdate()
    {
        int SceneID = SceneManager.GetActiveScene().buildIndex;

        if (SceneID == 1)
        {
            playerTurn.gameObject.SetActive(false);
        }
        else playerTurn.gameObject.SetActive(true);
    }
}
