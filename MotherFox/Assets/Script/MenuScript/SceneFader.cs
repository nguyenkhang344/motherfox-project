﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneFader : MonoBehaviour {

    public Image img;
    public AnimationCurve curve;
    public Slider slider;

    void Start()
    {
        StartCoroutine(FadeIn());
    }
    public void FadeTo(string scene)// string nay se duoc nhap tu cac ham goi fadeTo trong cac scripts khac
    {
        StartCoroutine(FadeOut(scene));//truyen string cua FadeTo vao FadeOut;
    }
    IEnumerator FadeIn()
    {
        float t = 1f;

        while (t > 0f)
        {
            float a = curve.Evaluate(t);//alpha = he so cua curve duoc tinh theo t
            t -= Time.deltaTime;//* them thong so  speed de thay doi thoi gian chuyen canh
            img.color = new Color(0f, 0f, 0f,a);// thong so alpha duoc thay bang t qua do alpha thay doi theo thoi gian
            yield return 0; // ham nay giup du khung hinh o fame 1 cho toi khi t<0 thi chuyen qua fame tiep theo
        }

    }
    IEnumerator FadeOut(string scene)
    {
        float t = 0f;

        while (t <1f)
        {
            float a = curve.Evaluate(t);//alpha = he so cua curve duoc tinh theo t
            t += Time.deltaTime;//* them thong so  speed de thay doi thoi gian chuyen canh
            img.color = new Color(0f, 0f, 0f, a);// thong so alpha duoc thay bang t qua do alpha thay doi theo thoi gian
            yield return 0; // ham nay giup du khung hinh o fame 1 cho toi khi t<0 thi chuyen qua fame tiep theo
        }
        //SceneManager.LoadScene(scene);// string scene nay duoc hung tu FadeOut 
        StartCoroutine(LoadAsynchronously(scene));
    }
    IEnumerator LoadAsynchronously(string scence)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(scence);
        while (!operation.isDone)
        {
            slider.gameObject.SetActive(true);
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            slider.value = progress;
            //Debug.Log(progress);

            yield return null;
        }
    }
}
