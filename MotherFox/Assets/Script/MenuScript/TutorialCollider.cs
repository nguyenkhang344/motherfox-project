﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCollider : MonoBehaviour {

    public Transform hideTut;
    private int DisplayLimit=0;
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9 && DisplayLimit < 1) 
        {
            hideTut.gameObject.SetActive(true);
            Time.timeScale = 0;
            DisplayLimit++;
        }
    }
}
