﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnIcon : MonoBehaviour {

    private int maxHeartAmount=10;
    public int startHeart;
    public int curHealth;
    private int maxHealth;
    private int healthPerHeart = 1;
    public Image[] healthImages;
    public Sprite[] healthSprite;
    private Transform watchMenu;
    private bool menuClose=false;
    void Start()
    {
        maxHealth = maxHeartAmount * healthPerHeart;
        checkHealthAmount();
        watchMenu = transform.GetChild(1);
    }
    void FixedUpdate()
    {
        if (gameOverScript.watchAd)
        {
            watchMenu.gameObject.SetActive(true);
            gameOverScript.watchAd = false;
        }
        if (menuClose)
        {
            Invoke("closeMenu", 1.5f);
            menuClose = false;
        }

        startHeart = LivesScritpt.initialLives;
        curHealth = LivesScritpt.CurLives * healthPerHeart;
       checkHealthAmount();
    }
    void checkHealthAmount()
    {
        for(int i = 0; i < maxHeartAmount; i++)
        {
            if (startHeart <= i)
            {
                healthImages[i].enabled = false;
            }
            else
            {
                healthImages[i].enabled = true; 
            }
        }
        UpdateHeart();
    }
    void UpdateHeart()
    {
        bool empty = false;
        int i = 0;
        foreach(Image image in healthImages)
        {
            if (empty)
            {
                image.sprite = healthSprite[0];
            }
            else
            {   // moi i tuong duong mot o mau
                i++;
                // phia duoi la giai quyet van de o mau tiep theo > so mau hien co
                if(curHealth>=i*healthPerHeart)
                {
                    image.sprite = healthSprite[healthSprite.Length - 1];
                    //sprit 0= 0 mau
                    //sprite 1 nua mau
                    // sprite 2 day mau
                    // nhu vay healthSprite[healthSprite.Length (3) - 1 =2] == tuc la goi sprite so 2 la sprite day mau
                }
                else
                {
                    int currentHeartHealth = (int)(healthPerHeart - (healthPerHeart * i - curHealth));
                    // cong thuc tren de tim xem currentHeatHealth = hay < i*healthPerHeart neu nho hon thi nho hon bao nhiu
                    //neu khong lay ket qua tru cho healthPerHeart thi khi dem chia cho healthPerImage se =2 thi  imageindex=2 , healthSrite[2] day mau vay la du mot mau
                    int healthPerImage = healthPerHeart / (healthSprite.Length - 1);
                    // lam sao de healthperImage=1
                    // healthSprite.Length-1 tuc la day goi sprite  day mau 
                    //sprite day mau / dem chia cho so mau moi trai tim = duoc so mau moi sprite
                    // gia su healthPerHeart la 4 ma ta chi co 3 sprite thi 4/(3-1) =2 tuc moi sprite dai dien 2 mau
                    int imageIndex = currentHeartHealth / healthPerImage;
                    //-------------------------------------------------
                    // gia su curhealth=7
                    // theo cong thuc phia tren ta tinh duoc currentHeartHealth=(2-(2*i(4)-7)=1 tuc la con thieu 1 de du them mot Heart
                    //theo cong thuc imageIndex= lay so doi ra / so mau moi sprite dai dien
                    // tuc la imageIndex=1/1
                    //image.sprite=healthSprite[1]= sprite nua mau
                    image.sprite = healthSprite[imageIndex];
                    empty = true;
                }
            }
        }
    }
    public void takeDamage(int amount)
    {
        curHealth += amount;
        curHealth = Mathf.Clamp(curHealth, 0, startHeart * healthPerHeart);
        UpdateHeart();
    }
    public void AddHeartContainer()
    {
        startHeart++;
        startHeart = Mathf.Clamp(startHeart, 0, maxHeartAmount);
        //curHealth = startHeart * healthPerHeart;
        //maxHealth = maxHeartAmount * healthPerHeart;
        checkHealthAmount();
    }
  
    public void MenuOut(bool clicked)
    {
        if (watchMenu.gameObject.activeInHierarchy)
        {
            Animator anim = watchMenu.GetComponent<Animator>();
            anim.SetBool("ADout", true);
            menuClose = true;
        }
    }
    void closeMenu()
    {
        watchMenu.gameObject.SetActive(false);
    }
}
