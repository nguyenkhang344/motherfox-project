﻿
using UnityEngine;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour {
    [SerializeField]
    private string hoverSound = "ButtonHover";

    AudioManager audioManager;

    public SceneFader fader;
    public Button[] levelButton;
    
    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }
      
        int levelReached = PlayerPrefs.GetInt("levelReached",1);
        // PlayerPrefs giu trong tinh ng choi khi dang xuat voiv key la levelReached va default la 1
        Debug.Log(levelReached);
        for (int i = 0; i < levelButton.Length; i++)
        {
            if(i+1>levelReached)// vi i=0 ma level default lai =1 nen ta cong them 1 vao i  vay khi i lon hon levelReached thi khoa i do lai (level thap thi ko
                //choi duoc mang cao )
            levelButton[i].interactable = false;
        }
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.L))
        {
            PlayerPrefs.DeleteKey("levelReached");
            Debug.Log("resetkey");
        }
    }
    public void Select(string levelName)
    {
        if (LivesScritpt.CurLives > 0)
        {
            audioManager.PlaySound(hoverSound);
            fader.FadeTo(levelName);
        }
        else gameOverScript.watchAd = true;
    }
    public void BackMainMenu(string mainMenu)
    {
        fader.FadeTo(mainMenu);
    }
}
