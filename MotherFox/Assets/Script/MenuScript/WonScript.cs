﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonScript : MonoBehaviour {

    [HideInInspector]public bool wonGame=false;
    public Transform reward;
	void Start () {
        wonGame = true;
	}
	
	// Update is called once per frame
	void Update () {
        wonGame = true;
	}
    public void backReward()
    {
        reward.gameObject.SetActive(false);
    }
}
