﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScript : MonoBehaviour {
    private GameObject moveTut;
    private GameObject hideTut;
    private GameObject jumpTut;
    private GameObject SneakyTut;
    private GameObject Instructor;
    private GameObject attackTut;
    void Start()
    {
        moveTut = transform.GetChild(0).gameObject;
        hideTut = transform.GetChild(1).gameObject;
        jumpTut = transform.GetChild(2).gameObject;
        SneakyTut = transform.GetChild(3).gameObject;
        Instructor = transform.GetChild(4).gameObject;
        attackTut = transform.GetChild(5).gameObject;
    }
    public void TurnOffMoveTut()
    {
        moveTut.SetActive(false);
    }
    public void TurnOffHideTut()
    {
        hideTut.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
    public void TurnOffJumpTut()
    {
        jumpTut.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
    public void TurnOffSneaky()
    {
        SneakyTut.SetActive(false);
        Time.timeScale = 1;
    }
    public void TurnOffInstuctor()
    {
        Instructor.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
    public void TurnOffAttackTut()
    {
        attackTut.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
