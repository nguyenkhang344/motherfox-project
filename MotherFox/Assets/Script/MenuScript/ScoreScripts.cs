﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreScripts : MonoBehaviour {
    public int MaxScore;
    public int PlayerScore;
    public Text ScoreText;
    public Text highScore;
    public Text highScore2;
    public WonScript wonMenu;
    public Transform reward;

    public bool countScore;

    //soundCaching
    
    public string cheering= "cheering";
    AudioManager audioManager;

    [SerializeField]
    private string HighScoreLevel;
    private int FinalScore;
    // Use this for initialization
    void Start () {
        HighScoreLevel = SceneManager.GetActiveScene().name.ToString();

        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }

    }

    // Update is called once per frame
    void FixedUpdate () {

        countScore = wonMenu.wonGame;
        if (countScore)
        {
            FinalScore =MaxScore- PlayerScore;

        }
        HighScore();
        CountScore();
        if (Input.GetKey(KeyCode.X))
        {
            Reset();
            Debug.Log("resetScore");
        }
    }
    void CountScore()
    {
      
        ScoreText.text = FinalScore.ToString();
        highScore.text = PlayerPrefs.GetInt(HighScoreLevel, FinalScore).ToString();
        highScore2.text = PlayerPrefs.GetInt(HighScoreLevel, FinalScore).ToString();
       
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    void HighScore()
    {
        if (FinalScore > PlayerPrefs.GetInt(HighScoreLevel, 0))
        {
            PlayerPrefs.SetInt(HighScoreLevel, FinalScore);
         //   highScore.text = FinalScore.ToString();
            reward.gameObject.SetActive(true);
            audioManager.PlaySound(cheering);
        }
    }
    void Reset()
    {
        PlayerPrefs.DeleteKey(HighScoreLevel);
    }

}
