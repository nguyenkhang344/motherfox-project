﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    [SerializeField]
    private string hoverSound = "ButtonHover";
    [SerializeField]
    private string hoverSound1 = "buttonHover1";

    AudioManager audioManager;
    public Image muteAudio;
    public Image muteMusic;


    private Transform optionMenu;
    private bool closemenu;

    public SceneFader fader;
	// Use this for initialization
	void Start () {
        optionMenu = transform.GetChild(4);

        audioManager = AudioManager.instance;
        if(audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }
        SetStateSound();
        SetStateMusic();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void FixedUpdate()
    {
        if (closemenu)
        {
            Invoke("closeOption", 1f);
            closemenu = false;
        }
    }
    public void Play(string levelName) 
    {
        audioManager.PlaySound(hoverSound);
        fader.FadeTo(levelName);
    }
    public void ToogleSound()
    {
        if (PlayerPrefs.GetInt("AudioMuted", 0) == 0)
        {
            PlayerPrefs.SetInt("AudioMuted", 1);      
        }
        else
        {
            PlayerPrefs.SetInt("AudioMuted", 0);
          
        }
        SetStateSound();
    }
 
    private void SetStateSound()
    {
        if (PlayerPrefs.GetInt("AudioMuted", 0)==0)
        {
            muteAudio.gameObject.SetActive(false);
        }
        if (PlayerPrefs.GetInt("AudioMuted", 0) == 1)
        {
            muteAudio.gameObject.SetActive(true);
        }
    }
    public void musicToggle()
    {
        audioManager.PlaySound(hoverSound1);
        if (PlayerPrefs.GetInt("MusicMuted", 0) == 0)
        {
            PlayerPrefs.SetInt("MusicMuted", 1);
        }
        else
        {
            PlayerPrefs.SetInt("MusicMuted", 0);

        }
        SetStateMusic();
    }
    private void SetStateMusic()
    {
        if (PlayerPrefs.GetInt("MusicMuted", 0) == 0)
        {
            muteMusic.gameObject.SetActive(false);
        }
        if (PlayerPrefs.GetInt("MusicMuted", 0) == 1)
        {
            muteMusic.gameObject.SetActive(true);
        }
    }
    public void OptionMenuIn(bool clicked)
    {
        optionMenu.gameObject.SetActive(true);
    }
    public void OptionMenuOut()
    {
        if (optionMenu.gameObject.activeInHierarchy)
        {
            Animator anim = optionMenu.GetComponent<Animator>();
            anim.SetBool("Close", true);
            closemenu = true;
        }
    }
    private void closeOption()
    {
        optionMenu.gameObject.SetActive(false);
    }
    public void Quitgame()
    {   
        Debug.Log("has quit");
        Application.Quit();
    }
}
