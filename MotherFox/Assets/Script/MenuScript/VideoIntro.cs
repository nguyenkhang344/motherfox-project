﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoIntro : MonoBehaviour
{
    public Transform SkipButton;

    [SerializeField]
    private string movie = "intro.mp4";
    public SceneFader sceneFader;
    [SerializeField]
    private string FadetoLevel;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(streamVideo(movie));
    }
    private IEnumerator streamVideo(string video)
    {
        Handheld.PlayFullScreenMovie(video, Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFill);
        yield return new WaitForEndOfFrame();
        Debug.Log("The Video playback is now completed.");
        sceneFader.FadeTo(FadetoLevel);
    }
    public void skipScene()
    {
        SkipButton.gameObject.SetActive(false);
        sceneFader.FadeTo(FadetoLevel);

    }
}