﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class gameCondition : MonoBehaviour {
    private static int counts;
    private static int countlive;
   
 
    public Transform wonMenu;
    public Transform GUI;
    public Transform GameOverUI;

    public Text countText;

    private Animator animUI;
    [SerializeField]
    private int countEnough;
    void Start()
    {
        setCountText();
        GUI = GameObject.Find("ButtonInGame").transform;
      
    }
    void FixedUpdate()
    {
        counts = enemy_Health.countDeath;
        setCountText();
        if (counts >= countEnough)
        {
            wonMenu.gameObject.SetActive(true);             //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
            GUI.gameObject.SetActive(false);
           
        }
        countlive = enemy_Health.countAlive;
        if (countlive >= 1)
        {
            animUI = GameOverUI.GetComponent<Animator>();
            if (!GameOverUI.gameObject.activeInHierarchy)
            {
                GameOverUI.gameObject.SetActive(true);
                animUI.SetBool("Open", true);
                
                
    
            }
            GUI.gameObject.SetActive(false);
        }
    }
    void setCountText()
    {
         countText.text = counts.ToString()+" / "+countEnough.ToString();
    }
   
}
