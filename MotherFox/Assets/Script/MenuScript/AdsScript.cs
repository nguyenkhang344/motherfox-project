﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsScript : MonoBehaviour {
    public Transform networkError;
    void Start()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize("1541649", true);
        }
    }

    public void ShowAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Advertisement.Show("rewardedVideo", new ShowOptions() { resultCallback = HandleAdResult });
        }
        else networkError.gameObject.SetActive(true);
    }

    private void HandleAdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                LivesScritpt.CurLives += LivesScritpt.initialLives;
                Debug.Log("player gain +10 turns");
                break;
            case ShowResult.Skipped:
                Debug.Log("player not watch hold Ad");
                break;
            case ShowResult.Failed:
                networkError.gameObject.SetActive(true);
                Debug.Log("ad is failed to show, check out internet");
                break;
        }
    }
    public void OkButton()
    {
        networkError.gameObject.SetActive(false);
    }
}
