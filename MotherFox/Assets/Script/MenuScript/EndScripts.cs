﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScripts : MonoBehaviour {
    AudioManager audioManager;
    public SceneFader sceneFader;
    public Transform exit;
    public Transform ending;
    public Transform credits;
    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }
    }
    public void endingScene()
    {
        ending.gameObject.SetActive(false);
        credits.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);
        audioManager.PlaySound("ButtonHover");
    }
    public void exitScene(string levelToLoad)
    {
        sceneFader.FadeTo(levelToLoad);
    }
}
