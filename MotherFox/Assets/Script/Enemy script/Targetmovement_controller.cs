﻿using UnityEngine;
using System.Collections;

public class Targetmovement_controller : MonoBehaviour
{
    public float enemyspeed;
    Animator enemyAnimator;
    //facing 
    public GameObject enemyGraphic;
    bool canFlip = true;
    bool facingRight = true; // con moi cung chieu voi main_character set = true
    float flipTime = 5f;
    float nextFlipChance = 0f;
    //attacking
    public float chargeTime;
    float startChargeTime;
    bool charging;
    Rigidbody2D enemyRB;

    // Use this for initialization
    void Start()
    {
        enemyAnimator = GetComponent<Animator>();
        enemyRB = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextFlipChance)
        {
            if (Random.Range(0, 10) >= 5) flip();
            nextFlipChance = Time.time + flipTime;
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (facingRight && other.transform.position.x < transform.position.x) flip(); // chinh huong xoay con moi khi player vao vung trigger
            else if (!facingRight && other.transform.position.x > transform.position.x) flip();
            canFlip = false;
            charging = true;
            startChargeTime = Time.time + chargeTime; // canh thoi gian lao vao
        }
    }
    void OnTriggerStay2D(Collider2D other) //code cho con moi lao vao
    {
        if (other.tag == "Player")
        {
            if (startChargeTime < Time.time + chargeTime)
            { // Time.time + chargeTime = thoi gian nhan vat dung trong vong
                if (!facingRight) enemyRB.AddForce(new Vector2(1, 0) * enemyspeed);
                else enemyRB.AddForce(new Vector2(-1, 0) * enemyspeed);
                enemyAnimator.SetBool("Running", charging);
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            canFlip = true;
            charging = false;
            enemyRB.velocity = new Vector2(0f, 0f);
            enemyAnimator.SetBool("Running", charging);
        }
    }
    void flip()
    {  //dinh nghia cach xoay
        if (!canFlip) return;
        float facingX = enemyGraphic.transform.localScale.x;
        facingX *= -1f;
        enemyGraphic.transform.localScale = new Vector3(facingX, enemyGraphic.transform.localScale.y, enemyGraphic.transform.localScale.z);
        facingRight = !facingRight;


    }
}
