﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadLine : MonoBehaviour {
   
        AudioManager audioManager;

        public string trapSound = "trapSound";
        [SerializeField] int dmg;

        // Use this for initialization
        void Start()
        {
            audioManager = AudioManager.instance;
            if (audioManager == null)
            {
                Debug.LogError("no AudioManager found");
            }

        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.layer == 9)// other ko o trang thai trigger va other.compareTag la so sanh tag 
            {
                other.SendMessageUpwards("Damage", dmg);       // ham gui bien den other
                audioManager.PlaySound(trapSound);
            }
        }
    
}
