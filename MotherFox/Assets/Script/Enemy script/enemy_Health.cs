﻿using UnityEngine;
using System.Collections;

public class enemy_Health : MonoBehaviour {
    public int curHealth;
    private bool stop;
    public  static int countDeath;
    public static int countAlive;
    private enemy_patrolling patroll;
    private enemySight sight;
    Animator anim;
    Rigidbody2D rigi;

    AudioManager audioManager;
    public string bunnyScream = "bunnyScream";


    // Use this for initialization
    void Start()
    {

        patroll = gameObject.GetComponent<enemy_patrolling>();
        sight = gameObject.GetComponent<enemySight>();
        anim = gameObject.GetComponent<Animator>();
        rigi = gameObject.GetComponent<Rigidbody2D>();
        countDeath = 0;
        countAlive = 0;
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }

    }
    void FixedUpdate()
    {
        if (curHealth <= 0)
        {
            stop=true;
            patroll.enabled = false;
            sight.enabled = false;
            anim.SetBool("death", true);
        

            // goi thay cho instance
            GameObject obj = effectScript.current.GetPooledObject();
            if (obj == null) return;
            obj.transform.position = transform.position;
            obj.SetActive(true);
            if (transform.lossyScale.x < 0)
            {
                obj.transform.localScale = new Vector3(obj.transform.lossyScale.x * -1, obj.transform.localScale.y, obj.transform.localScale.z);
            }
            obj.SetActive(true);
        }
        if (stop) rigi.velocity = Vector3.zero;
    }

    public void Damage(int damage) // Damage la ten duoc dat o ham sendMassagesUpwards
    {
        curHealth -= damage; // -= nghia la tru di mot so bang 1 bien
    } 
    void destroy()
    {
        audioManager.StopSound(bunnyScream);
        transform.gameObject.SetActive(false);
        countDeath = countDeath + 1;
    }
    public void goHide(bool hide)
    {
        if (hide)
        {
            stop = true;
            anim.SetBool("hide", true);
            patroll.enabled = false;
            sight.enabled = false;
        }
    }
    void goHideinCave()
    {
        countAlive = countAlive + 1;
        audioManager.StopSound(bunnyScream);
        transform.gameObject.SetActive(false);
    }
}
