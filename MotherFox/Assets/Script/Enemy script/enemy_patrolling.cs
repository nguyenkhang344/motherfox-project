﻿using UnityEngine;
using System.Collections;

public class enemy_patrolling : MonoBehaviour {

    public enum moveType {UseTransform,UsePhysics };
    public moveType moveTypes;

    public ScoreScripts Scoring;
    public Transform[] pathPoints;
    public int curPath = 0;
    public float reachDistance = 5f;

    public float moveSpeed = 5f;

    private Rigidbody2D rigi;
   

    public float pauseTime;
    
    private float curTime;

    // Use this for initialization
    void Start () {
        Scoring = GameObject.Find("Scoring").GetComponent<ScoreScripts>();
        rigi = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        switch (moveTypes)
        {
            case moveType.UseTransform:
                UseTransform();
                break;
            case moveType.UsePhysics:
                UsePhysics();
                break;
        }
        
    }
    void UseTransform()
    {
            Vector3 dir = pathPoints[curPath].position - transform.position;       // diem den tu waypoint den vi tri nha vat
            Vector3 dirNorm = dir.normalized;
            transform.Translate(dirNorm * moveSpeed);       // ham khoi chay tu diem nhan vat den waypoint
            if (dir.magnitude <= reachDistance && (Time.time - curTime) >= pauseTime)
            {
                curPath++;
                curTime = 0;
                if (curPath >= pathPoints.Length)               //do lon so diem da den  hien tai lon hon tong so diem den thi reset curpath=0)
                {
                    curPath = 0;
                    //curTime = 0; neu set curTime = 0 tai day thi bunny di tu diem A den B ngung va ko nghi tai diem A
                }
            }
            if (curTime == 0)
            {
                curTime = Time.time;
            }
    }
    void UsePhysics()
    {
        Vector3 dir = pathPoints[curPath].position - transform.position;       // diem den tu waypoint den vi tri nha vat
        Vector3 dirNorm = dir.normalized;
        rigi.velocity = new Vector2(dirNorm.x * moveSpeed , rigi.velocity.y);    // ham khoi chay tu diem nhan vat den waypoint
        if (dir.magnitude <= reachDistance && (Time.time - curTime) >= pauseTime) // thoi gian troi di time.time - thoi gian da luu curtime nho hon pause time tuc chua doi du 7s thi ko lam gi het
        {
            Scoring.PlayerScore ++;
            curPath++;
            curTime = 0;
            if (curPath >= pathPoints.Length)               //do lon so diem da den  hien tai lon hon tong so diem den thi reset curpath=0)
            {
                curPath = 0;
                //curTime = 0; neu set curTime = 0 tai day thi bunny di tu diem A den B ngung va ko nghi tai diem A
            }
        }
        if (curTime == 0)
        {
            curTime = Time.time;
        }
    }
    void OnDrawGizmos() // ve cac diem way point
    {
        if (pathPoints == null)
            return;
        foreach (Transform pathPoint in pathPoints)
        {
            if (pathPoint)
            {
                Gizmos.DrawSphere(pathPoint.position,reachDistance);
            }
        }
    }

}
