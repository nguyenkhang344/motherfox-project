﻿using UnityEngine;
using System.Collections;

public class enemySight : MonoBehaviour {
    enemy_behavior behavior;
    public float waitTime;
    public Transform bunnyIcon;

    [SerializeField]
    Color hearColor;
    [SerializeField]
    Color runColor;

    private bool playerRun;
    private bool playerInLeft;
    private bool playerInright;
    Rigidbody2D rigi;
    public float moveSpeed = 5f;

    public int time;
    enemy_patrolling patroll;

    private bool playerInRange;
   
    SpriteRenderer spr;

    [SerializeField]
    Color RabbitColor;
    [SerializeField]
    Transform lightOfSightEnd;
    Transform player;

    private GameObject PlayerState;


    // caching
    AudioManager audioManager;
    public string bunnyScream = "bunnyScream";

    // Use this for initialization
    void Start () {
        bunnyIcon = gameObject.transform.GetChild(1);
        spr = gameObject.GetComponent<SpriteRenderer>();
        rigi = GetComponent<Rigidbody2D>();
        behavior = gameObject.GetComponent<enemy_behavior>();
        playerInRange = false;
        player = GameObject.Find("Character_animation").transform;
        PlayerState = GameObject.Find("Character_animation");
        patroll = GetComponent<enemy_patrolling>();

        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }

    }
    void FixedUpdate()
    {
        spr.color = RabbitColor;
        if (canPlayerBeSeen())
        {
            audioManager.PlaySound(bunnyScream);
            // dat cac hanh dong vao day
        }
        else
        {
            spr.color = Color.white;
        }
        if (time >= 1)
        {
            
            StartCoroutine(runAway());
        }
    }
    bool canPlayerBeSeen()
    {
        if (playerInRange)
        {
            bunnyIcon.gameObject.SetActive(true);
            if ((playerRun || playerInFieldOfView()) && time < 1 && PlayerState.activeInHierarchy)
            {
                time = time + 1;
                patroll.enabled = false;            // tat componentscript enemy_patrolling
                return true;
            }
            else return false;    
        }
        bunnyIcon.gameObject.SetActive(false);
        return false;
    }
	void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.tag == ("Player") || other.transform.tag == ("crouch")||other.transform.tag==("playerRun"))
        {
           
            playerInRange = true;
            
            // khi da trong vong collider gia tri da gan luc dau dc so voi
            //thoi gian thuc Tim.time cho den khi Tim.time tang den mot muc lon hon runtime
            // muc dich de khi ra khoi brush ko bi kiem tra collider tag ngay lap tuc

            if (other.transform.tag == ("playerRun")) playerRun = true;
                if (other.transform.position.x < transform.position.x)  //dinh vi tri player
                {
                    playerInLeft = true;
                    playerInright = false;
                }
                if (other.transform.position.x > transform.position.x)
                {
                    playerInLeft = false;
                    playerInright = true;
                }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == ("Player") || other.transform.tag == ("crouch"))
            playerInRange = false;
    }

    bool playerInFieldOfView()
    {
        Vector2 directionToPlayer = player.position - transform.position; // ke duong thang tu bunny den ng choi
        Debug.DrawLine(transform.position, player.position, Color.magenta);

        Vector2 lineOfSight= (lightOfSightEnd.position - transform.position); // ke duong tu bunny den diem cuoi cua tam nhin
        Debug.DrawLine(transform.position, lightOfSightEnd.position,Color.yellow);

        float angle = Vector2.Angle(directionToPlayer, lineOfSight); //goc hop boi duong ke tu bunny den ng choi va bunny den diem cuoi tam nhin
        if (angle < 65 )          //neu goc hop boi 2 duong ke  1 tu nhan vat  , 1 tu diem cuoi ma nho hon 65 độ bunny co the thay ng choi
            return true;
        else
            return false;

    }
	// Update is called once per frame
	void Update ()
    {

    }
    IEnumerator runAway()
    {
        yield return new WaitForSeconds(waitTime);
        bunnyIcon.gameObject.SetActive(true);
        bunnyIcon.gameObject.GetComponent<Animator>().SetBool("run", true);
       
        float move = 1;
            if (playerInLeft)
            {
            rigi.velocity = new Vector2(move * moveSpeed, rigi.velocity.y);//rigi.AddForce(new Vector2(1, 0) * moveSpeed);
            }
            if (playerInright)
            {
            rigi.velocity = new Vector2(-move * moveSpeed, rigi.velocity.y);
            }
            transform.gameObject.tag = "bunnyRun";
        }
}
