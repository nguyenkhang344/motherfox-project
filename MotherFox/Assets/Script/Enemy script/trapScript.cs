﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapScript : MonoBehaviour {
    // caching
    AudioManager audioManager;

    public string trapSound = "trapSound";
    public GameObject trapEffect;
    Animator anim;
    [SerializeField]
     int dmg;
    // Use this for initialization
    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if ( other.gameObject.layer==9)// other ko o trang thai trigger va other.compareTag la so sanh tag 
        {
            other.SendMessageUpwards("Damage", dmg);       // ham gui bien den other
            anim.SetTrigger("catch");
            Instantiate(trapEffect, other.transform.position, other.transform.rotation);

            audioManager.PlaySound(trapSound);
        }
    }
}

