﻿using UnityEngine;
using System.Collections;

public class enemy_behavior : MonoBehaviour {
    private Rigidbody2D rigi;
    private Animator anim;
    private bool facingRight = true;

    // Use this for initialization
    void Start () {
        rigi = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
        float move = rigi.velocity.x;
        if (move > 0 && facingRight) flip();
        if (move < 0 && !facingRight) flip();
        anim.SetFloat("speed", Mathf.Abs(move));
    }
   public void flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
