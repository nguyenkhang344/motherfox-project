﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class ApplySprite : MonoBehaviour {
    private MeshRenderer myMS;
    [SerializeField]
    private int layerOrder;

    public Sprite sprite;
    Renderer rend;
    void Start()
    {
        myMS = transform.GetComponent<MeshRenderer>();
        myMS.sortingLayerName = "Platforms";
        myMS.sortingOrder = layerOrder;
        var croppedTexture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
        var pixels = sprite.texture.GetPixels((int)sprite.rect.x,
                                             (int)sprite.rect.y,
                                             (int)sprite.rect.width,
                                             (int)sprite.rect.height );

        // luu y dung sprite.rect thay cho sprite.textureRect vi
        // Sprite rect va textureRect co the khong = nhau dan den xung dot phat sinh loi
        croppedTexture.SetPixels(pixels);
        croppedTexture.Apply();


        rend = GetComponent<MeshRenderer>();
        rend.sharedMaterial.mainTexture = croppedTexture;
      
    }
}
