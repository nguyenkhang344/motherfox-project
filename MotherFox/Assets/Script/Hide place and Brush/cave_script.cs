﻿using UnityEngine;
using System.Collections;

public class cave_script : MonoBehaviour {
    public bool hide;
    // Use this for initialization
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.isTrigger!=true && other.tag == "bunnyRun")
        {
            other.SendMessageUpwards("goHide", hide = true);
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.isTrigger != true && other.tag == "bunnyRun")
        {
            other.SendMessageUpwards("goHide", hide = true);
        }
    }
}
