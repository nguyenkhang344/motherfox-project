﻿using UnityEngine;
using System.Collections;


public class brush_hide : MonoBehaviour {
    GameObject Player;
    Animation animate;
    Animator playerAnim;
    Renderer Rend;
    public float starTime;
    public float waitTime;
    public Color defaultColor;
    public Color selectecColor;
    public Color EnterColor;
    public ControllerCenter ButtonInGame;
    public GameObject hideButton;

    private GameObject hideEffect;
    private bool resetTime;
   // private GameObject hideIcon;
    private bool Playerexist;
   // private hide_icon isHide;
    private ControllerCenter joyStick;
   
	// Use this for initialization
	void Start () {
        ButtonInGame = GameObject.Find("ButtonInGame").GetComponent<ControllerCenter>();
        hideEffect = transform.GetChild(0).gameObject;
        Player = GameObject.Find("Character_animation");
        playerAnim = Player.GetComponent<Animator>();
        Rend = GetComponent<MeshRenderer>();

        //  hideIcon = gameObject.transform.GetChild(0).gameObject;
        hideButton.SetActive(false);
      //  hideIcon.SetActive(false);
     //   isHide = hideIcon.GetComponent<hide_icon>();
        joyStick = ButtonInGame.GetComponent<ControllerCenter>();

    }
	
	// Update is called once per frame
	void Update () {
      
    }
    void FixedUpdate()
    {
        

        //Rend.material.color = new Color32(198, 198, 198, 255);

        if (resetTime)
        {
            starTime = 0;
            resetTime = false;
        }
        if (Input.GetKeyDown(KeyCode.E)|| !joyStick.isHide()||Player.GetComponent<MainDeath>().isDead)
        {
            Rend.material.color = defaultColor;
            Player.SetActive(true);
            Playerexist = false;
            playerAnim.SetBool("hide", false);
            //hideEffect.SetActive(true);
          
        }
        if (Playerexist)
        {
            hideButton.SetActive(true);
            //   hideIcon.SetActive(true); 
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == ("Player") || other.tag == ("crouch") || other.tag == ("playerRun"))
        {
            hideButton.SetActive(true);
            //  hideIcon.SetActive(true);
            starTime = Time.time + waitTime;
           
                hideEffect.SetActive(true);      
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
       
        if (other.tag == ("Player") || other.tag == ("crouch")|| other.tag == ("playerRun"))
        {

            Rend.material.color = EnterColor;
            if (Input.GetKeyDown(KeyCode.E) ||joyStick.isHide() && Time.time  > starTime)
            {
                Rend.material.color = selectecColor;
                playerAnim.SetBool("hide", true);
                resetTime = true;
                Playerexist = true;
                hideEffect.SetActive(true);
            }
        }
     
    }
    private void OnTriggerExit2D(Collider2D other)
    {
       
        if (other.tag == ("Player") || other.tag == ("crouch") || other.tag == ("playerRun"))
        {
            //   if (!Player.activeInHierarchy) hideIcon.SetActive(true);
            // hideIcon.SetActive(false);
            hideButton.SetActive(false);
          
        }
    }
}
