﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode] //thi hanh trong editor
public class WavePlane : MonoBehaviour
{
    [SerializeField]
    private int layerOrder; 

    private Vector3[] meshVertices;
    private Vector3[] origVerticesPos;
    public int[] moveVertices; //for my mesh, I used 0 and 1 (upper two vertices)
    public float waveSpeed = 1;
    public float waveAmplitudeX = 0.001f;
    public float waveAmplitudeY = 0;
    private float xMove;
    private Mesh mesh;
    private float randomize;
    
    void Awake()
    {
        MeshRenderer myMS = this.GetComponent("MeshRenderer") as MeshRenderer;
        MeshFilter myMF = this.GetComponent("MeshFilter") as MeshFilter;
        mesh = myMF.sharedMesh;
        meshVertices = mesh.vertices;
        origVerticesPos = meshVertices;
        randomize = Random.Range(0, 100);

        myMS.sortingLayerName = "Platforms";
        myMS.sortingOrder = layerOrder;
    }

    void Update()
    {
        if (Time.timeScale == 1)
        {
            xMove = Mathf.Sin((Time.time * waveSpeed) + randomize);
            foreach (int i in moveVertices)
            {
                meshVertices[i] = origVerticesPos[i] + new Vector3(xMove * waveAmplitudeX / transform.localScale.x, xMove * waveAmplitudeY, 0);
            }

            mesh.vertices = meshVertices;
            mesh.RecalculateNormals();// thuc hien vertices reflect
        }
        else mesh.RecalculateNormals();// du cho mesh ko bi keo khi timescale ve 0
    }
}