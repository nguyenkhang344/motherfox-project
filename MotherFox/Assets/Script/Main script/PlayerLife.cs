﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class PlayerLife : MonoBehaviour
{
    public TimeSpan timeShow;
    public Text textTime;
    private const int MAX_Lives = 8;
   
    private static TimeSpan newLifeInterval = new TimeSpan(0,0,150);
    // tao interval voi timespan la 5s, tuc la 5s hoi mot mang

    DateTime lostLifeStamp;
    // danh dau het mang
    private int amountOfIntervalPassed;
    private long temp;
    // tong so mang duoc hoi
    void Start()
    {
        if (PlayerPrefs.GetInt("FirstStart") == 0)
        {
            PlayerPrefs.SetInt("FirstStart", 1);
            PlayerPrefs.SetString("lostLifeStamp", System.DateTime.Now.ToString());
        }
        Debug.Log(PlayerPrefs.GetInt("FirstStart"));
    }
    void Update()
    {
        DateTime.TryParse(PlayerPrefs.GetString("lostLifeStamp"), out lostLifeStamp);
        if (LivesScritpt.CurLives < MAX_Lives)
        {
            TimeSpan t = DateTime.Now - lostLifeStamp;
            //Debug.Log(lostLifeStamp);
            //Debug.Log(t);
            TimeSpan leftTime =new TimeSpan(0,0,150*MAX_Lives);
            timeShow = leftTime - t;
            Debug.Log(timeShow);

            // thoi gian troi qua t = thoigian hien tai- tru di thoi gian danh dau het mang
            // theo ly thuyet neu doi trong nhieu nam  thi tong thoi gian cho se rat lon voi 
            // kieu bien int
            try
            {
                //lam tron hoac them mang moi khi qua mot nua thoi gian doi
                double intervalD = System.Math.Floor(t.TotalSeconds / newLifeInterval.TotalSeconds);
                // ta dung kieu du lieu double thay cho float vi mien du lieu lon co the
                // luu dc bien lon theo yeu cau cua ly thuyet da neu tren
                amountOfIntervalPassed = Convert.ToInt32(intervalD);
                // ta lay thoi gian troi qua chia cho thoi gian hoi mang = amountOfIntervalPassed
                //amo
            }

            // ----ta dung khoi xu ly ngoai le try/catch--
            // vi voi dieu kien if liveleft<MAX_lives ta se thuc hien try de tim so mang con lai tuy nhien
            // khi so mang con lai vuot qua MAX_lives thi ta su dung catch de bat ngoai le tran(OverflowException) de dua
            // so mang con lai ve bang MAX_Lives
            catch (OverflowException)
            {
                LivesScritpt.CurLives = MAX_Lives;
                
            }
            if (amountOfIntervalPassed + LivesScritpt.CurLives >= MAX_Lives)
            {
                // de so luot doi ra khong bi cong don ta dung
                //amountOfIntervalPassed = 0;

                LivesScritpt.CurLives = MAX_Lives;

            }
           // Debug.Log("it's been" + t + "since lives dropped from full(now" + LivesScritpt.CurLives + ")." + amountOfIntervalPassed + "reload passed. Live left" + getAmountOfLives());

        }
    }
    void FixedUpdate()
    {
        if (textTime != null)
        {
            setTimeText();
        }
        else return;
    }
    [ContextMenu("Lose Life")]
    void lostLife()
    {
        if (LivesScritpt.CurLives--==MAX_Lives)
        {
            //lostLifeStamp = DateTime.Now;
            PlayerPrefs.SetString("lostLifeStamp", System.DateTime.Now.ToString());
           // temp = Convert.ToInt64(PlayerPrefs.GetString("lostLifeStamp"));
        }
    }
    int getAmountOfLives()
    {
        return Mathf.Min(LivesScritpt.CurLives + amountOfIntervalPassed, MAX_Lives);
    }
    void setTimeText()
    {
        string stringTime = string.Format("{0:00}:{1:00}", timeShow.Minutes, timeShow.Seconds);
        // ta su dung string.format voi cu phap {0:00} cho phut ung voi timeshow.Minutes {1:00} cho giay ung voi timeshow.Seconds
        textTime.text = stringTime;
    }
}