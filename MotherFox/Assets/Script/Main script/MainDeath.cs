﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainDeath : MonoBehaviour {
    private Animator animUI;
   
    private int PlayerLife;

    public bool isDead;
    public Transform GameOverUI,GUI;
    public ParticleSystem deathEffect;
    public int CurHealth;
    Foxmothercontrollerscript liveAction;
    Animator anim;
    Rigidbody2D rigi;
    // caching
    AudioManager audioManager;
    public string foxDeathSound = "foxDeathSound";

    // camera shake
    [SerializeField]
    private float shakeAmount, shakeLength;
    public CameraShake cameraShake;

    // Use this for initialization
    void Start () {
        rigi = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        liveAction = gameObject.GetComponent<Foxmothercontrollerscript>();

        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }


        if (cameraShake == null)
        {
            Debug.LogError("no camera Shake");
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
    private void FixedUpdate()
    {
           
        if (CurHealth <= 0)
        {

            cameraShake.Shake(shakeAmount, shakeLength);
            rigi.velocity = new Vector2(0f, rigi.velocity.y);     
            animUI = GameOverUI.GetComponent<Animator>();       
            //rigi.velocity = Vector2.zero;
            anim.SetLayerWeight(1, 1);
            anim.SetBool("death", true);
            isDead = true;
            liveAction.enabled = false;
            StartCoroutine(GameOverOn());
        }
       
    }
    public void Damage(int damage)
    {
        CurHealth -= damage;
    }
    IEnumerator GameOverOn()
    {

        GUI.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        if (!GameOverUI.gameObject.activeInHierarchy)
        {
            GameOverUI.gameObject.SetActive(true);
            animUI.SetBool("Open", true);
           
        }
    }
    void DeathEffect()
    {
        audioManager.PlaySound(foxDeathSound);
        if (isDead) Instantiate(deathEffect, transform.position, transform.rotation);
    }
}
