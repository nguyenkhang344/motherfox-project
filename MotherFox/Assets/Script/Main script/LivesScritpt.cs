﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesScritpt : MonoBehaviour {

    public static int initialLives=8;
    private static int curLives;
    void Start()
    {
      
            curLives = PlayerPrefs.GetInt("playerLife", initialLives);
      
    }
    public static int CurLives
    {
        get{ return curLives; }
        set
        {
            curLives = Mathf.Max(0, value);// de giu so mang khong am
            PlayerPrefs.SetInt("playerLife", curLives);
        }
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.R))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
