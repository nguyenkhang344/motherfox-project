﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum TriggerButton
{
    None=0,
    Attack=1,
}
public class ControllerCenter : MonoBehaviour
{
    [SerializeField]
    private string hoverSound = "buttonHover1";
    [SerializeField]
    private string grassHide = "grassHide";

    AudioManager audioManager;


    public Transform RetryMenu, GUI;
    private bool left;
    private bool right;
    private bool attack;

    public TriggerButton buttons;
    private Animator anim;
    private bool hides;
    
    // Use this for initialization
    private void Start()
    {

        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }
    }
    private void Update()
    {
        buttons = TriggerButton.None;
        if (attack) {
            
            buttons = TriggerButton.Attack;
            attack = false;
        }
        else buttons = TriggerButton.None;
   
    }
    public void LeftDown()
    {
        left = true;
       
    }
    public void LeftUp()
    {
        left = false;
        
    }
    public void RightDown()
    {
        right = true;
       
    }
    public void RightUp()
    {
        right = false;
       
    }
    public void attackButton()
    {
        attack = true;
       
    }
    public void attackButtonUp() {
        attack = false;
    }

    public float horizontal()
    {
        if (right) return 1;
        if (left) return -1;       
        if (!left && !right) return 0;
        if (left && right) return -1;
        else return 0;
        
    }
    public bool attacking()
    {
        if (attack) return true;
        else return false;
    }
    public void retryMenu(bool clicked)
    {
        anim = RetryMenu.GetComponent<Animator>();
   
        if (clicked == true) {
        if (!RetryMenu.gameObject.activeInHierarchy) RetryMenu.gameObject.SetActive(true);
            anim.SetBool("Open",true);
            GUI.gameObject.SetActive(false);

            audioManager.PlaySound(hoverSound);
        }
        
        else{
            GUI.gameObject.SetActive(true);
            anim.SetBool("Open", false);
            
        }
     
    }
    public void resumeGame()
    {
        audioManager.PlaySound(hoverSound);
        Time.timeScale = 1;
    }
    public void hide(bool clicked)
    {
        audioManager.PlaySound(grassHide);
        hides = !hides;
      
    }
    public bool isHide()
    {
        if (hides) return true;
        else return false;
    }
    public bool isTriger(TriggerButton trigger)
    {
        return (buttons & trigger) == trigger;
    }
        
}

