﻿using UnityEngine;
using System.Collections;

public class Foxmothercontrollerscript : MonoBehaviour {
    public ControllerCenter joyStick;
    public bool iscrouch;
    private bool stopRun;
    private bool hidden;
    private bool attacking;
    public float attackTimer;
    public float attackCd;
    public TrailRenderer attackVfx;
    public TrailRenderer attackVfxland;


    public float moveSpeed=10f;
    public float jumpForce=500f;
    public float kneelingSpeed = 0.05f;
    public Collider2D attackBox;

    public float stayJump;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    bool facingRight = true;
    bool grounded ;
    float groundRadius = 0.2f;


    private float move;
    private bool jumpHoldon;
    public Rigidbody2D rigi;
    private Animator anim;

    public string jumpAttack = "jumpAttack";
    public string landAttack = "landAttack";
    

    // caching
     AudioManager audioManager;

    // camera shake
    [SerializeField]
    private float shakeAmount, shakeLength;
    public CameraShake cameraShake;

    void Awake()
    {
      
        rigi = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        attackBox.enabled = false;
        attackVfx.enabled = false;
        attackVfxland.enabled = false;
     
      
    }
	// Use this for initialization
	void Start () {

        if (cameraShake == null)
        {
            Debug.LogError("no camera Shake");
        }

        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
    }
    void FixedUpdate()
    {
       
        //||Input.GetKey(KeyCode.Space)
        anim.SetFloat("vSpeed", rigi.velocity.y);
        if (Swipe_script.Instance.IsSwiping(SwipeDirection.Tap) && grounded && !attacking && !iscrouch && !jumpHoldon)
        {
            if (move != 0)
            {
                grounded = false;
                rigi.AddForce(new Vector2(0, jumpForce));
            }
            if (move == 0)
            {
                if (facingRight)
                {
                    grounded = false;
                    rigi.AddForce(new Vector2(stayJump, jumpForce));
                }
                if (!facingRight)
                {
                    grounded = false;
                    rigi.AddForce(new Vector2(-stayJump, jumpForce));
                }
            }

        }

        //anim.SetBool("kneeling", false);
        transform.gameObject.tag = "Player";
        anim.SetBool("ground", grounded);
        if ( Input.GetKey(KeyCode.Space)||joyStick.isTriger(TriggerButton.Attack) && !grounded&!attacking)//Swipe_script.Instance.IsSwiping(SwipeDirection.Dou) ||
        {
            anim.SetBool("jumpAttack", true);
            attacking = true;
            attackTimer = attackCd;
            attackBox.enabled = true;
            attackVfx.enabled = true;

            audioManager.PlaySound(jumpAttack);
            cameraShake.Shake(shakeAmount, shakeLength);
        }
        if ( joyStick.isTriger(TriggerButton.Attack) && grounded && !attacking)//Swipe_script.Instance.IsSwiping(SwipeDirection.Dou) ||
        {
            anim.SetBool("attack", true);
            attacking = true;
            attackTimer = attackCd;
            attackBox.enabled = true;
            attackVfxland.enabled = true;
            //??   grounded = false; 
            audioManager.PlaySound(landAttack);
            cameraShake.Shake(shakeAmount, shakeLength);

        }
        if (attacking)
        {
            if (attackTimer > 0)
            {
                attackTimer -= Time.deltaTime;

            }
           else
            {
                attacking = false;
                anim.SetBool("attack", false);
                attackBox.enabled = false;
                attackVfx.enabled = false;
                attackVfxland.enabled = false;
            }
        }

        if (!grounded && rigi.velocity.y < 0) anim.SetBool("land", true);
        if (grounded)
        {
            anim.SetBool("jumpAttack", false);
            anim.SetBool("land", false);
        }
       // if (!grounded) return;
        //----------tu day chi thuc hien nhung hanh dong tren mat dat----------------
        move =joyStick.horizontal() ;
        anim.SetFloat("Speed", Mathf.Abs(move));
        rigi.velocity = new Vector2(move* (moveSpeed*Time.fixedDeltaTime), rigi.velocity.y);
        if (move != 0)
        {
            if (!audioManager.isPlaying("grassWalk"))
            {
                audioManager.PlaySound("grassWalk");
            }
           
            transform.gameObject.tag = "playerRun";
        }
        else audioManager.StopSound("grassWalk");
        if (move > 0 && !facingRight)Flip();
        if (move < 0 && facingRight) Flip();

        if (Swipe_script.Instance.IsSwiping(SwipeDirection.Down)||Input.GetKey(KeyCode.LeftControl) && !iscrouch)
        {
            iscrouch = true;  // bien dung de giu khi tu bui ra ko bi doi tag thanh player
            anim.SetBool("crouching", true);
            rigi.velocity = new Vector2(0f, rigi.velocity.y);
            transform.gameObject.tag = "crouch";  // chuyen tag thanh crouch
        }
        if (Swipe_script.Instance.IsSwiping(SwipeDirection.Tap)||Input.GetKey(KeyCode.X) && iscrouch)
        {
            jumpHoldon = true;
            iscrouch = false;
            anim.SetBool("crouching", false);
            //anim.SetBool("kneeling", false);
        }
        if (jumpHoldon)
        {
            StartCoroutine(jumpHolder());
        }
        if (iscrouch&& move!=0)
        {         
            rigi.velocity = new Vector2(move * kneelingSpeed, rigi.velocity.y);
            //anim.SetBool("kneeling", true);
            transform.gameObject.tag = "crouch";   // chuyen tag
        }
        if (stopRun) move = 0;
       
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = gameObject.transform.localScale;
        theScale.x *= -1;
        gameObject.transform.localScale = theScale;
    }

    void hideAction()
    {       
        gameObject.SetActive(false);
        iscrouch = false;
    }
    void jumptoBrush()
    {
        stopRun = true;
        if (facingRight)
        rigi.AddForce(new Vector2(moveSpeed, 0));
        if(!facingRight)
        rigi.AddForce(new Vector2(-moveSpeed, 0));
    }

    IEnumerator jumpHolder()
    {
        yield return new WaitForSeconds(0.1f);
        jumpHoldon = false;
    }

}
