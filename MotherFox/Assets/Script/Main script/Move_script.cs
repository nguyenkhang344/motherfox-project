﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Move_script : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {
    private Vector3 inputVector;
    public void OnPointerDown(PointerEventData data)
    {
        if (gameObject.name == "Left")
        {
            inputVector = new Vector3(-1, 0, 0);
        }
        else  inputVector= new Vector3(1, 0, 0);
        
    }
    public void OnPointerUp(PointerEventData data)
    {
        if (gameObject.name == "Left")
        {
            inputVector = Vector3.zero;
        }
        else inputVector = Vector3.zero;
       
    }
	// Use this for initialization
	public float horizontal()
    {
        return inputVector.x;
    }
}
