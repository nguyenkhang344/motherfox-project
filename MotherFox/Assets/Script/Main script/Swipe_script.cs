﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//using UnityEngine.EventSystems;

public enum SwipeDirection
{
    None=0,
    Left=1,
    Right=2,
    Up=4,
    Down=8, 
    Dou= 16,
    Tap=64,
}

public class Swipe_script : MonoBehaviour {

    // Use this for initialization
    private static Swipe_script instance;
    public static Swipe_script Instance{get{return instance;}}
    //khai bao static de goi script ma khong can component  

    public SwipeDirection Direction { set; get; }
    private int swipeId;
    private Vector3 touchPos;
    private float swipeResistanceX = 50f;
    private float swipeResistanceY = 100f;
    private void Start()
    {
        instance = this;
    }
    void Update()
    {
        Direction = SwipeDirection.None;

    
        if (Input.touchCount > 0)
        {
           
            foreach (Touch touch in Input.touches)
            {
              

                if (touch.fingerId == swipeId)
                {
                    if (touch.phase == TouchPhase.Ended)
                    {

                        // if (EventSystem.current.currentSelectedGameObject != null) return ; dung de ngan ko cho thuc hien touch trong vung button
                        Vector3 endPos = touch.position;
                        Vector2 deltaSwipe = touchPos - endPos;
                        if (Mathf.Abs(deltaSwipe.x) > swipeResistanceX)
                        {
                            //swipe on the X axis
                            Direction |= (deltaSwipe.x < 0) ? SwipeDirection.Right : SwipeDirection.Left;  //? = new , : = neu ko

                        }
                        if (Mathf.Abs(deltaSwipe.y) > swipeResistanceY)
                        {
                            //swipe on the y axis
                            Direction |= (deltaSwipe.y < 0) ? SwipeDirection.Up : SwipeDirection.Down;
                        }
                        if ( Mathf.Abs(deltaSwipe.y) < swipeResistanceY)
                        {
                            if (EventSystem.current.currentSelectedGameObject != null) return;
                            Direction = SwipeDirection.Tap;
                        }
                    }
                }
                if (touch.tapCount == 2)  // ham tapcount chuyen  dung de kiem tra tap , ko nen dung touchcount
                {
                    if (EventSystem.current.currentSelectedGameObject != null) return;
                    Direction = SwipeDirection.Dou;
                }
                if (touch.phase == TouchPhase.Began)
                {
                    touchPos = touch.position;
                    swipeId = touch.fingerId;

                }
            }
         }
    }
    public bool IsSwiping(SwipeDirection dir)
    {
        return (Direction & dir) == dir;
    }
   
}
