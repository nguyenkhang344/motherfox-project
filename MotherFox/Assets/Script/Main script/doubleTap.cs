﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class doubleTap : MonoBehaviour {
    int TapCount;
    bool attack=false;
    float newTime;
    bool readyForDoubleTap;
    public float interval;
    void Start()
    {
        
    }
    void Update()
    {
        if (Input.touchCount == 1)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Ended)
                {
                    
                    TapCount += 1;
                }
                if (TapCount == 1)
                {
                    StartCoroutine(DoubleTapInterval());
                }
                else if (TapCount == 2 && readyForDoubleTap)
                {
                    //do something
                    attack = true;
                  

                    TapCount = 0;
                    readyForDoubleTap = false;
                }
              
              
            }
        }
    }
    public bool attackButton()
    {
        if (attack) return true;
        else return false;
    }
    IEnumerator DoubleTapInterval()
    {
        yield return new WaitForSeconds(interval);
        readyForDoubleTap = true;
      
    }
}
