﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SystemTime : MonoBehaviour {
    DateTime oldTime;
    DateTime curTime;

    private int maxLives=3;
    [SerializeField]
    private int curLives;

    private TimeSpan timePerTurn=new TimeSpan(0,0,5);
    private TimeSpan passedTime;
    private int turnReload;
    private double timeReload;
    private bool save;
	// Use this for initialization
	void Start () {
      
    
    }
	
	// Update is called once per frame
	void Update () {
        DateTime.TryParse(PlayerPrefs.GetString("lostLifeStamp"), out oldTime);
        if (curLives ==0)
        {
            
            curTime = System.DateTime.Now;
            passedTime = curTime - oldTime;
            timeReload = passedTime.TotalSeconds/ timePerTurn.TotalSeconds;

            turnReload = Convert.ToInt32(timeReload);
            if (curLives + turnReload > maxLives)
            {
                curLives = maxLives;
               
            }
        }
        Debug.Log(curLives+"it's been"+passedTime);
	}
     void SaveTime()
     {
        if (save)
        {
            
            PlayerPrefs.SetString("lostLifeStamp", System.DateTime.Now.ToString());
            save = false;
        }
     }
    public void DecreaLive(int n)
    {
        curLives -= n;
    }
}
