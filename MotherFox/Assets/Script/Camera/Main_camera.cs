﻿using UnityEngine;
using System.Collections;

public class Main_camera : MonoBehaviour {
    public Transform target;

    public float widthDistance;
    public float smoothing;
    Vector3 widthOffset;
    Vector3 offset;
    float facing;
    float lowY;

	// Use this for initialization
	void Start () {
    
       
        offset = transform.position - target.position;
        lowY = transform.position.y;
	}
    private void Update()
    {
        Vector3 theScale = target.localScale;
        facing = theScale.x;


     
    }

    // Update is called once per frame
    void FixedUpdate () {

           if (facing < 0)
        {
            widthOffset = new Vector3(widthDistance*-1, 0, 0);
            
        }
        else widthOffset = new Vector3(widthDistance, 0, 0);

        Vector3 targetCamPos = target.position + offset + widthOffset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
        if (transform.position.y < lowY) transform.position = new Vector3(transform.position.x, lowY, transform.position.z);
	
	}
}
