﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicinGame : MonoBehaviour {
    public static MusicinGame instance;
    [SerializeField]
    private bool playMusic;
    [SerializeField]
    Sound[] sound;
    public int minTime;
    public int maxTime;

    private bool MusicisPlaying;
    private float curTime;
    private Sound playSound;
    private int playTime;
    // caching
    // Use this for initialization
    void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
    void Start()
    {
        for (int i = 0; i < sound.Length; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + sound[i].name);
            _go.transform.SetParent(this.transform);
            sound[i].setSource(_go.AddComponent<AudioSource>());
        }
        playSound = sound[0];
        playTime = Random.Range(minTime, maxTime);

    }

    // Update is called once per frame
    void Update()
    {
       
        if (!playSound.Playing())
        {

            if (curTime >= playTime)
            {
                RandomPlay();
                curTime = 0;
                playTime = Random.Range(minTime, maxTime);
            }
            curTime += Time.deltaTime;
        }
       // Debug.Log(playTime);

        if ((PlayerPrefs.GetInt("MusicMuted", 0) == 1))
        {
            foreach (Sound sounded in sound)
            {
                sounded.Mute();
            }
        }
        else
        {
            foreach (Sound sounded in sound)
            {
                sounded.NormalSound();
            }
        }
        

    }
    void FixedUpdate()
    {
       

    }
    void RandomPlay()
    {
        playSound = sound[Random.Range(0, sound.Length)];
        playSound.Play();
    }
}
