﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSound : MonoBehaviour {
    [SerializeField]
    private bool playBirdSound;
    [SerializeField]
    Sound[] sound;
    public float minTime;
    public float maxTime;

    private float curTime;

    // caching
    // Use this for initialization
    void Start () {
    for(int i=0; i< sound.Length; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + sound[i].name);
            _go.transform.SetParent(this.transform);
            sound[i].setSource(_go.AddComponent<AudioSource>());
        }
    }

    // Update is called once per frame
    void Update () {
        if ((PlayerPrefs.GetInt("AudioMuted", 0) == 1))
        {
            foreach (Sound sounded in sound)
            {
                sounded.Mute();
            }
        }
        else return;

    }
    void FixedUpdate()
    {
        float playTime = Random.Range(minTime, maxTime);
        curTime += Time.deltaTime; ;
        if (playBirdSound)
        {
            if (curTime >= playTime)
            {

                curTime = 0;
                sound[Random.Range(0, sound.Length)].Play();
            }
        }
    }
}
