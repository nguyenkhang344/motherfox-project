﻿using System.Collections;
using UnityEngine;
[System.Serializable] // phai bat serializable cho class nay de array sound cua AudioManager co the truy xuat class nay
public class Sound
{
    public string name;
    public AudioClip clip;
    [Range (0f,1f)]         // range giup tao gioi han cho float va tao mot slidbar
    public float volume=0.7f;
    [Range(0.5f,1.5f)]
    public float pitch=1f;
    [Range(0f,0.5f)]
    public float randomVolume = 0.1f;//random de lam cho am thanh ko qua giong nhau
    [Range(0f, 0.5f)]
    public float randomPitch = 0.1f;


    public bool loop = false;

    private AudioSource source;

    public void setSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
        source.loop = loop;
    }
    public void Play()
    {
        source.volume = volume*(1+ Random.Range(-randomVolume/2f,randomVolume/2f));
        source.pitch = pitch*(1+Random.Range(-randomPitch/2f,randomPitch/2f));
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }
    public bool Playing()
    {
        if (source.isPlaying) return true;
        else return false;
    }
    public void Mute()
    {
        source.volume = 0;
    }
    public void NormalSound()
    {
        source.volume = volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f));
    }
}

public class AudioManager : MonoBehaviour {
    public static AudioManager instance;       // ta dung static bool de tao reference cho AuidoManager de no co the dc instance o voi awake , va cho tat ca cac script khac quyen 
                                                // truy cap va play clip tu audio manager

    [SerializeField]
    Sound[] sounds; // mang sound duoc truy xuat tu class sound
    void Awake()// ta dung awake de danh thuc ham aduio manager
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

    }

    void Start()
    {
        // chung ta dung for loop de khoi tao cac gameobject chua audio source ma ta dua gia tri vao tu array sound
        for (int i = 0; i < sounds.Length; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + sounds[i].name);
            _go.transform.SetParent(this.transform);
            // day la compact scripts thay cho
            //AuidoSource _source=_go.AddComponent<AudioSource>();
            //sound[i].SetSource(_source)
            // nhung cach duoi giup han che store variable nen chung ta se khong phai don rac collector la _source
            sounds[i].setSource (_go.AddComponent<AudioSource>());
        }
        PlaySound("Music");
    }
    void Update()
    {
        if ((PlayerPrefs.GetInt("AudioMuted", 0) == 1))
        {
            foreach(Sound sounded in sounds)
            {
                sounded.Mute();
            }
        }
        else return;
    }
    public void PlaySound(string _name)
    {
        for(int i=0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {       //statement kiem tra xem name ta nhap vao co trong thu vien name cua sound ko
                sounds[i].Play();
                return;

            }
        }
        //no sound with name 
        Debug.LogWarning("AudioManager: Sound not found in list:" + _name);
    }

    public void StopSound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {       //statement kiem tra xem name ta nhap vao co trong thu vien name cua sound ko
                sounds[i].Stop();
                return;

            }
        }
        //no sound with name 
        Debug.LogWarning("AudioManager: Sound not found in list:" + _name);
    }
    public bool isPlaying(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                if (sounds[i].Playing()) return true;
                else return false;
            }
        }
        Debug.LogWarning("AudioManager: Sound not found in list:" + _name);
        return false;
    }
         
}
