﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputTouch : MonoBehaviour {

    public LayerMask touchInputMask;
    //private List<GameObject> touchList = new List<GameObject>();
    //private GameObject[] touchesOld;
    private RaycastHit hit;
    // Update is called once per frame
    void Update()
    {

#if UNITY_EDITOR
#endif
        // phan tren dung de khi play trong editor thi cho phep vua cam ung va dung chuot tuy nhien phai chinh sua code  
        if (Input.touchCount >0)
        {
          //  touchesOld = new GameObject[touchList.Count]; // dat so luong mang touchesOld bang so luong cua mang touchlist
          //  touchList.CopyTo(touchesOld);// chuyen du lieu tu mang touchlist sang cho touchold
          //  touchList.Clear();//xoa het du lieu trong touchlist
            foreach (Touch touch in Input.touches)
            {
                GameObject camera = GameObject.Find("Main Camera");
                Ray ray = camera.GetComponent<Camera>().ScreenPointToRay(touch.position);
              
                if (Physics.Raycast(ray, out hit, touchInputMask))
                {
                    GameObject recipient = hit.transform.gameObject;
                    // dinh nghia vat hit la recipent

                    //RaycastHit hit; da duoc doi di

                //    touchList.Add(recipient);// nhap recipient vao touchlist
                   
                    if (touch.phase == TouchPhase.Began)
                    {
                        recipient.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
                        //DontRequireReceiver=neu ko co script nao nhan message nay thi bo qua(ko bao loi)
                    }
                    if (touch.phase == TouchPhase.Ended)
                    {
                        recipient.SendMessage("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
                    }

                    if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    {
                        recipient.SendMessage("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
                    if (touch.phase == TouchPhase.Canceled)
                    {
                        recipient.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
                        // truong hop dat qua  nhiu ngon tay va mun tat cam ung
                    }
                }
            }
           // foreach(GameObject g in touchesOld)
            {
               // if (!touchList.Contains(g))
                {
                 //   g.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);

                   // y tuong la tao mot touchlistold de luu cac touch o trang thai dau den trang thai tiep theo se so sanh
                   //trang thai dau voi trang thai sau(touchlist) neu o trang thai sau ko co message touch nhu o trang thai dau thi tha trigger cam ung
                   // vi du nhan vao nut nhung ko tha ra ma keo ra ngoai vung bam thi bth se tiep tuc phase move nhung nho ham nay thi khi keo
                   //ra khoi vung bam se tu dong tha trigger cam unmg ra thay vi ghi nhan ham move
                }
            }
        }
	}
}
