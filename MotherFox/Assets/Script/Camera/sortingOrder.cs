﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sortingOrder : MonoBehaviour {
    [SerializeField]
    private int LayerOrder;
    void Start()
    {
        MeshRenderer myMS = this.gameObject.GetComponent<MeshRenderer>();
        myMS.sortingLayerName = "Platforms";
        myMS.sortingOrder = LayerOrder;
    }
}
