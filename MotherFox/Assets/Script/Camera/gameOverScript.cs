﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameOverScript : MonoBehaviour {
    [SerializeField]
    private string hoverSound = "buttonHover2";

    AudioManager audioManager;


    // public bool resetScore;
    public SceneFader sceneFader;
    public string LevelToLoad = "LevelSelector";
    public string nextLevel;
    public int levelToUnlock;
    public static bool watchAd ;

    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }

    }
    public void Retry(bool clicked)
    {
        if (LivesScritpt.CurLives > 0)
        {
            watchAd = false;
            LivesScritpt.CurLives -=1;
            PlayerPrefs.SetString("lostLifeStamp", System.DateTime.Now.ToString());
            //   resetScore = true;
            //  Debug.Log(resetScore);
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            sceneFader.FadeTo(SceneManager.GetActiveScene().name);
        }
        else
        {
            watchAd = true;
        }
    }
    public void Menu()
    {
      //  resetScore = true;
        sceneFader.FadeTo(LevelToLoad);
    
    }
   public void pauseGame()
   {
    
     Time.timeScale = 0;
    }
   void dissapear()
    {
        gameObject.SetActive(false);
    }
    public void winGame()
    {
        PlayerPrefs.SetInt("levelReached", levelToUnlock);
        sceneFader.FadeTo(nextLevel);
        audioManager.PlaySound(hoverSound);
    }
 
}
