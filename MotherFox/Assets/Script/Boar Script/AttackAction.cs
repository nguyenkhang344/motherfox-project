﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu (menuName =("PluggableAI/Actions/Attack"))]
public class AttackAction : Actions {
    
    public override void Act(StateController controller)
    {
        Attack(controller);
    }
    private void Attack(StateController controller)
    {
        if (controller.chaseTarget != null)
        {

            controller.attack.Attack(controller);
           
                Vector3 DistanceToChaser = controller.transform.position - controller.chaseTarget.position;
                float DistanceToChaserAbs = Mathf.Abs(DistanceToChaser.x);
             if (controller.chaseTarget.gameObject.activeSelf)
            {
                if (DistanceToChaserAbs < 0.01) controller.chaseTarget.SendMessageUpwards("Damage", controller.enemyStats.damage);
            }
        }
    }
}
