﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("PluggableAI/Decision/Attack"))]
public class AttackDecision : Decision {
    public override bool Decide(StateController controller)
    {
        bool attacked = isAttack(controller);
        return attacked;
    }
  
		
	private bool isAttack(StateController controller)
    {
        Vector2 DistancetoChaser = controller.chaseTarget.position - controller.transform.position;
        float DistancetoChaserAbs = Mathf.Abs(DistancetoChaser.x);

        if (DistancetoChaserAbs <= controller.enemyStats.attackRange)
        {
            return true;
        }
        return false;
    }
}
