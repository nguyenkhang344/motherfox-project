﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = ("PluggableAI/Actions/Attack1"))]
public class AttackAction1 : Actions {

    public override void Act(StateController controller)
    {
        Attack(controller);
    }
    private void Attack(StateController controller)
    {
        Vector2 DistancetoChaser = controller.chaseTarget.position - controller.transform.position;
        float DistancetoChaserAbs = Mathf.Abs(DistancetoChaser.x);

        if (DistancetoChaserAbs <= controller.enemyStats.attackRange)
        {


            controller.attack.Attack(controller);
        }
        else controller.attack.StopAttack(controller);
        if (controller.chaseTarget.gameObject.activeSelf)
        {
            if (DistancetoChaserAbs < 1) controller.chaseTarget.SendMessageUpwards("Damage", controller.enemyStats.damage);
        }
    }
}
