﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("PluggableAI/Decision/Scan"))]
public class ActiveStateDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool isChaserEscape = ChaserEscapse(controller);
        if (isChaserEscape)
        {
            controller.audioManager.StopSound("boarAttack");
        }
        return isChaserEscape;
     
    }
    private bool ChaserEscapse (StateController controller)
    {
        if (controller.chaseTarget.gameObject.activeSelf)
        {
            Vector2 DistancetoChaser = controller.eyes.transform.position - controller.chaseTarget.position;
            float DistancetoChaserAbsx = Mathf.Abs(DistancetoChaser.x);
            float DistancetoChaserAbsy = Mathf.Abs(DistancetoChaser.y);
            Debug.DrawRay(controller.transform.position, controller.transform.forward.normalized * controller.enemyStats.lookRange, Color.cyan);
            Debug.DrawRay(controller.transform.position, controller.transform.up.normalized * controller.enemyStats.lookRange / 3, Color.yellow);
            if ((DistancetoChaserAbsx >= controller.enemyStats.lookRange + 1) || (DistancetoChaserAbsy >= controller.enemyStats.lookRange / 3)||controller.chaseTarget.GetComponent<MainDeath>().isDead)
            {
                return true;
            }
            return false;
        }
        return true;
    }
}
