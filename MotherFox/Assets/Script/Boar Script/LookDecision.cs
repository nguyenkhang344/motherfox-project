﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("PluggableAI/Decision/Look"))]
public class LookDecision : Decision {
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }
    private bool Look(StateController controller)
    {

        Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.lookRange, Color.green);
        RaycastHit2D[] hits = Physics2D.CircleCastAll(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, controller.enemyStats.lookRange, LayerMask.GetMask("Player"));
        if (hits != null)
        {
            foreach (RaycastHit2D hit in hits)
            {
                controller.warningIcon.SetActive(true);
                controller.curTime += Time.deltaTime;
             
                if (controller.curTime >= controller.enemyStats.WaitAttack)
                {
                    controller.Scoring.PlayerScore+=4;
                    controller.curTime = 0;
                    controller.chaseTarget = hit.transform;
                    controller.audioManager.PlaySound("boarAttack");
                    return true;
                }
            }return false;
        }
        return false;
    }

}
