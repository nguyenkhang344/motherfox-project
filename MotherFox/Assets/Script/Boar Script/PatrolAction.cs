﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("PluggableAI/Actions/Patrol"))]
public class PatrolAction : Actions {
    public override void Act(StateController controller)
    {
        Patrol(controller);
    }
 
    private void Patrol(StateController controller)
    {
        controller.warningIcon.SetActive(false);
        if (controller.attack != null)
        {
            controller.attack.StopAttack(controller);
        }
        Vector3 Direction = controller.wayPoints[controller.curPoints].position - controller.transform.position;
        Vector3 DirectionNor = Direction.normalized;
        controller.rigi.velocity = new Vector2(DirectionNor.x * controller.enemyStats.moveSpeed, controller.rigi.velocity.y);
        
        if (Direction.magnitude <= controller.enemyStats.reachDistance)
        {
          
            controller.rigi.velocity = Vector3.zero;
            if (controller.curTime <=controller.enemyStats.pauseTime)
            {
               
                controller.curTime += Time.deltaTime;
                
            }
            if(controller.curTime>controller.enemyStats.pauseTime)
            {
                controller.Scoring.PlayerScore++;
                controller.curTime = 0;
                controller.curPoints++;
                if (controller.curPoints >= controller.wayPoints.Length)
                {
                    controller.curPoints = 0;
                }
              
            }
         
        }
    }
}
