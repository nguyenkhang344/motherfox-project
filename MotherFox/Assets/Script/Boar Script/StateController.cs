﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour {
    public State curState;
    public EnemyStats enemyStats;
    public Transform eyes;
    public AnimationsStats animStats;
    public Transform[] wayPoints;
    public State remainSate;
    public Complete.AttackBehavior attack;

    public Complete.WalkandFlip WalkFlip;
    public GameObject warningIcon;
    public ScoreScripts Scoring;
    [HideInInspector] public Animator anim;
    [HideInInspector] public float curTime;
    [HideInInspector] public int curPoints;
    [HideInInspector] public Transform chaseTarget;
    [HideInInspector] public float stateTimeElapsed;
    [HideInInspector] public Rigidbody2D rigi;
    // caching
    public AudioManager audioManager;

    void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
        {
            Debug.LogError("no AudioManager found");
        }

    }

    void Awake()
    {
        Scoring = GameObject.Find("Scoring").GetComponent<ScoreScripts>();
        warningIcon.SetActive(false);
        attack = GetComponent<Complete.AttackBehavior>();
        anim = GetComponent<Animator>();
        rigi = GetComponent<Rigidbody2D>();

       
    }
    void Update()
    {
        curState.UpdateState(this);

    }
    private void OnDrawGizmos()
    {
        if (wayPoints == null) return;
        foreach(Transform Point in wayPoints)
        {
            if (Point) Gizmos.DrawSphere(Point.position, enemyStats.reachDistance);
        }
        if(curState!=null&& eyes != null)
        {
            Gizmos.color = curState.sceneGizmoColor;
            Gizmos.DrawWireSphere(eyes.position,enemyStats.lookSphereCastRadius);
        }
    }
    public void TransitionToState(State nextState)
    {
        if (nextState != remainSate)
        {
            curState = nextState;
            OnExitState();
        }
    }
    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }
    private void OnExitState()
    {
        stateTimeElapsed = 0;
    }
 
}
