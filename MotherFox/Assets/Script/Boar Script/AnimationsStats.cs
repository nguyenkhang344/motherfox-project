﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = ("PluggableAI/Animation"))]
public class AnimationsStats : ScriptableObject {
    public string walk;
    public string attack;
}
