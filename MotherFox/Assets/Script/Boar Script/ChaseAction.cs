﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("PluggableAI/Actions/Chase"))]
public class ChaseAction : Actions {

    public override void Act(StateController controller)
    {
        Chase(controller);
    }
    private void Chase(StateController controller)
    {
        
        Vector3 Direction = controller.chaseTarget.position - controller.transform.position;
        Vector3 DirectionNor = Direction.normalized;
        controller.rigi.velocity = new Vector2(DirectionNor.x * controller.enemyStats.runSpeed, controller.rigi.velocity.y);
    }
}
