﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("PluggableAI/EnemyStats"))]
public class EnemyStats : ScriptableObject {

    public float moveSpeed;
    public float lookRange;
    public float reachDistance;
    public float pauseTime;
    public float lookSphereCastRadius;
    public float runSpeed;
    public float attackRange;
    public float damage;
    public GameObject warningIcon;
    public float WaitAttack;


		
	
}
