﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Complete
{
    public class WalkandFlip : MonoBehaviour
    {

        public AnimationsStats animStats;

        private bool facingRight;
        private Animator anim;
        private Rigidbody2D rigi;
        private float speed;
        private float curTime;

        [SerializeField]
        private float minTime;
        [SerializeField]
        private float maxTime;
        // caching
        AudioManager audioManager;


        void Start()
        {
            rigi = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();

            audioManager = AudioManager.instance;
            if (audioManager == null)
            {
                Debug.LogError("no AudioManager found");
            }
          
        }
        void Update()
        {

            speed = rigi.velocity.x;
            if (speed > 0 && facingRight) Flip();
            if (speed < 0 && !facingRight) Flip();
        }
        void FixedUpdate()
        {
            anim.SetFloat(animStats.walk, Mathf.Abs(speed));

            float randomTime = Random.Range(minTime, maxTime);
            if (curTime < randomTime)
            {
                curTime += Time.deltaTime;           
            }
            if (curTime >= randomTime)
            {
                audioManager.PlaySound("boarWalk");
                curTime = 0;
            }
           
        }
        public void Flip()
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
        
    }
}
